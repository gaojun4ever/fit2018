import json
import logging
import sys

from pathfinding.core.diagonal_movement import DiagonalMovement
from pathfinding.core.grid import Grid
from pathfinding.finder.a_star import AStarFinder
import params



finder = AStarFinder(diagonal_movement=DiagonalMovement.never)

logging.basicConfig(level=logging.INFO, 
                    filename='gaojun_game.log',
                    filemode='w',
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger("GAOJUN")
logger.info('Starting ...')

#Global Info
player_1_home = (28,1)
player_2_home = (28,38)
peer_brain_pose = ()
save_fields = [(0,19),(0,20),(1,19),(1,20)]
attack_range = {
    'AiBrain': [(0,0)],
    'OverLordSoldier': [(1,0),(-1,0),(0,1),(-1,0)],
    'ArmourSoldier': [(1,0),(-1,0),(0,1),(-1,0),(1,1),(1,-1),(-1,1),(-1,-1)]
}
player = 1
globalInfo = {}
globalMap = {}
localRobots = {}
peerRobots = {}
waitingList = []
attackList = []
robot_dst = []
initInfoAns = {}



def update_map_matrix(pos_list):
    map_matrix = []
    for row in range(30):
        map_matrix.append([1]*40)
    map_matrix[28][1] = 0
    map_matrix[28][38] = 0
    for p in pos_list:
        map_matrix[p[0]][p[1]] = 0
    return map_matrix


def sendRep(req, ans, interfaceName):
    rep = req
    rep["interface"]["interfaceName"] = interfaceName
    rep["interface"]['ans'] = ans
    
    rep_str = json.dumps(rep)
    logger.info(rep_str)
    sys.stdout.write(rep_str+'\n')
    sys.stdout.flush()

def attack_test(req, robot_obj, walk, attack=""):
    ans = {
        "attack": attack,
        "robottype": robot_obj['robottype'],
        "roleid": robot_obj['roleid'],
        "walk": walk
    }
    req['interface'].pop('para')
    sendRep(req, ans, "queryFightOrderRsp")
def findRobotById(waittingList, id):
    for robot in waitingList:
        if robot['roleid'] == id:
            return robot
def path_to_operation(start_point, paths):
    operations = []
    cur_point = start_point
    for p in paths:
        if p[1]-cur_point[0] == 0 and  p[0]-cur_point[1] == 0:
            continue
        elif p[1]-cur_point[0] == 1 and  p[0]-cur_point[1] == 0:
            operations.append("D")
        elif p[1]-cur_point[0] == -1 and  p[0]-cur_point[1] == 0:
            operations.append("U")
        elif p[1]-cur_point[0] == 0 and  p[0]-cur_point[1] == -1:
            operations.append("L")
        elif p[1]-cur_point[0] == 0 and  p[0]-cur_point[1] == 1:
            operations.append("R")
        cur_point = (p[1],p[0])
    return operations

def getAttackList(peerRobots, myRobot):
    my_pose = (myRobot['rowid'],myRobot['columnid'])
    robottype = myRobot['robottype']
    attack_list = []
    for robot in peerRobots:
        peer_pose = (robot['rowid'],robot['columnid'])
        if robottype == "AiBrain":
            break
        elif robottype == "OverLordSoldier":#霸王兵
            attack_pose = [(my_pose[0]+p[0],my_pose[1]+p[1]) for p in attack_range["OverLordSoldier"]]
            if peer_pose in attack_pose:
                attack_list.append(peer_pose)
        elif robottype == "ArmourSoldier":#铁甲兵
            attack_pose = [(my_pose[0]+p[0],my_pose[1]+p[1]) for p in attack_range["ArmourSoldier"]]
            if peer_pose in attack_pose:
                attack_list.append(peer_pose)

    return attack_list
        
def check_is_blocked(dst_point, map_matrix):
    if map_matrix[dst_point[0]][dst_point[1]] == 0:
        return True
    else:
        return False

def find_clost_dst(dst_point, map_matrix):
    for i in range(2):
        temp = [(i,0),(-i,0),(0,i),(-i,0),(i,i),(i,-i),(-i,i),(-i,-i)]
        for t in temp:
            if dst_point[0]+t[0] < 0 or dst_point[0]+t[0] > 29:
                continue
            if dst_point[1]+t[1] < 0 or dst_point[1]+t[1] > 39:
                continue       
            if map_matrix[dst_point[0]+t[0]][dst_point[1]+t[1]] == 1:
                close_pose = (dst_point[0]+t[0],dst_point[1]+t[1])
                return close_pose
    
    return None



cnt = 0
while True:
    line = sys.stdin.readline().strip()
    if not line:
        break
    req = json.loads(line)
    interfaceName = req['interface']['interfaceName']
    num_round = req['round']
    logger.info("recieve interfaceName: "+interfaceName)
    if interfaceName == "queryRobotInitInfoReq":
        logger.info(line)
        player = req['player']
        if player == 1:
            initInfoAns = params.initInfoAns1
            robot_dst = params.robot_dst1
        elif player  == 2:
            initInfoAns = params.initInfoAns2
            robot_dst = params.robot_dst2
        logger.info("update player info : set player %d"%(player))
        sendRep(req, initInfoAns, "queryRobotInitInfoRsp")

    if interfaceName == "queryFightOrderReq":
        cnt += 1
        logger.info(line)
        globalInfo = req['interface']['para']
        logger.info("num localrobotlist : %d"%(len(req['interface']['para']['localrobotlist'])))
        waitingList = []
        robot_pos_list = []
        for robot in req['interface']['para']['localrobotlist']:
            robot_pos_list.append((robot['rowid'],robot['columnid']))
            if not robot['havefight']:
                waitingList.append(robot)
            localRobots[robot['roleid']] = robot 
        for robot in req['interface']['para']['peerrobotlist']:
            robot_pos_list.append((robot['rowid'],robot['columnid']))
            # peerRobots[robot['roleid']] = robot 

        robot = waitingList[0]
        map_matrix = update_map_matrix(robot_pos_list)
        # logger.info("matrix row %d, col %d"%(len(map_matrix),len(map_matrix[1])))
        map_matrix[robot['rowid']][robot['columnid']] = 1
        map_grid = Grid(matrix=map_matrix)
        start_point = (robot['rowid'],robot['columnid'])
        
        if map_matrix[robot_dst[robot['roleid']][0]][robot_dst[robot['roleid']][1]] == 1:
            end_point = (robot_dst[robot['roleid']][0],robot_dst[robot['roleid']][1])
        else:
            end_point = find_clost_dst((robot_dst[robot['roleid']][0],robot_dst[robot['roleid']][1]),map_matrix)
            if end_point == None:
                end_point = start_point
        # logger.info("%d,%d => %d,%d"%(start_point[0],start_point[1],end_point[0],end_point[1]))

        start = map_grid.node(start_point[1],start_point[0])
        end = map_grid.node(end_point[1],end_point[0])
        path, runs = finder.find_path(start, end, map_grid)
        # logger.info(path)
        walk = ""
        if len(path) > 0:
            op_list = path_to_operation(start_point, path)
            walk = ''.join(op_list)
            if robot['roleid'] == 0 or robot['roleid'] > 8:
                walk=walk[:5]
            elif robot['roleid'] > 0 and robot['roleid'] < 9:
                walk=walk[:9]
        else:
            walk = ""
        attack_list = getAttackList(req['interface']['para']['peerrobotlist'],robot)
        attack = ""
        if len(attack_list) > 0:
            # attack = str((attack_list[0][1],attack_list[0][0]))
            attack = "%d,%d"%(attack_list[0][0],attack_list[0][1])
        if attack != "":
            walk=""
        logger.info("roleid %d move !"%(robot['roleid']))
        logger.info("attack : (%d,%d) => "%(robot['rowid'],robot['columnid']) + attack)
        logger.info("walk :" + walk)
        attack_test(req, robot, walk,attack)           
        if cnt == 15:
            cnt = 0


        


    
    