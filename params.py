robot_dst1 = [(1,19),(0, 20),(1,20),(2,20),(2,19),(3,19),(2,18),(2,17),(4,19),(27,37),(28,37),(29,27),(27,4),(28,3),(29,2)]

robot_dst2 = [(1,20),(0,19),(1,19),(2,19),(2,20),(3,20),(2,21),(2,22),(4,20),(27,2),(28,2),(29,2),(27,37),(28,37),(29,37)]


initInfoAns1 =  {
    "robotlist": [
        {
            "rowid": 25,
            "columnid": 4,
            "hp": 0,
            "robottype": "AiBrain",
            "roleid": 0
        },
        {
            "rowid": 25,
            "columnid": 3,
            "hp": 0,
            "robottype": "OverLordSoldier",#霸王兵
            "roleid": 1
        },
        {
            "rowid": 25,
            "columnid": 2,
            "hp": 0,
            "robottype": "OverLordSoldier",#霸王兵
            "roleid": 2
        },
        {
            "rowid": 26,
            "columnid": 4,
            "hp": 0,
            "robottype": "OverLordSoldier",#霸王兵
            "roleid": 3
        },

        {
            "rowid": 26,
            "columnid": 3,
            "hp": 0,
            "robottype": "OverLordSoldier",#霸王兵
            "roleid": 4
        },
        {
            "rowid": 26,
            "columnid": 2,
            "hp": 0,
            "robottype": "OverLordSoldier",#霸王兵
            "roleid": 5
        },
        {
            "rowid": 27,
            "columnid": 4,
            "hp": 0,
            "robottype": "OverLordSoldier",#霸王兵
            "roleid": 6
        },
        {
            "rowid": 27,
            "columnid": 3,
            "hp": 0,
            "robottype": "OverLordSoldier",#霸王兵
            "roleid": 7
        },
        {
            "rowid": 27,
            "columnid": 2,
            "hp": 0,
            "robottype": "OverLordSoldier",#霸王兵
            "roleid": 8
        },

        {
            "rowid": 28,
            "columnid": 4,
            "hp": 0,
            "robottype": "ArmourSoldier",#铁甲兵
            "roleid": 9
        },
        {
            "rowid": 28,
            "columnid": 3,
            "hp": 0,
            "robottype": "ArmourSoldier",#铁甲兵
            "roleid": 10
        },
        {
            "rowid": 28,
            "columnid": 2,
            "hp": 0,
            "robottype": "ArmourSoldier",#铁甲兵
            "roleid": 11
        },
        {
            "rowid": 29,
            "columnid": 4,
            "hp": 0,
            "robottype": "ArmourSoldier",#铁甲兵
            "roleid": 12
        },
        {
            "rowid": 29,
            "columnid": 3,
            "hp": 0,
            "robottype": "ArmourSoldier",#铁甲兵
            "roleid": 13
        },
        {
            "rowid": 29,
            "columnid": 2,
            "hp": 0,
            "robottype": "ArmourSoldier",#铁甲兵
            "roleid": 14
        }
    ]
}


initInfoAns2 =  {
    "robotlist": [
        {
            "rowid": 25,
            "columnid": 35,
            "hp": 0,
            "robottype": "AiBrain",
            "roleid": 0
        },
        {
            "rowid": 25,
            "columnid": 36,
            "hp": 0,
            "robottype": "OverLordSoldier",#霸王兵
            "roleid": 1
        },
        {
            "rowid": 25,
            "columnid": 37,
            "hp": 0,
            "robottype": "OverLordSoldier",#霸王兵
            "roleid": 2
        },
        {
            "rowid": 26,
            "columnid": 35,
            "hp": 0,
            "robottype": "OverLordSoldier",#霸王兵
            "roleid": 3
        },

        {
            "rowid": 26,
            "columnid": 36,
            "hp": 0,
            "robottype": "OverLordSoldier",#霸王兵
            "roleid": 4
        },
        {
            "rowid": 26,
            "columnid": 37,
            "hp": 0,
            "robottype": "OverLordSoldier",#霸王兵
            "roleid": 5
        },
        {
            "rowid": 27,
            "columnid": 35,
            "hp": 0,
            "robottype": "OverLordSoldier",#霸王兵
            "roleid": 6
        },
        {
            "rowid": 27,
            "columnid": 36,
            "hp": 0,
            "robottype": "OverLordSoldier",#霸王兵
            "roleid": 7
        },
        {
            "rowid": 27,
            "columnid": 37,
            "hp": 0,
            "robottype": "OverLordSoldier",#霸王兵
            "roleid": 8
        },

        {
            "rowid": 28,
            "columnid": 35,
            "hp": 0,
            "robottype": "ArmourSoldier",#铁甲兵
            "roleid": 9
        },
        {
            "rowid": 28,
            "columnid": 36,
            "hp": 0,
            "robottype": "ArmourSoldier",#铁甲兵
            "roleid": 10
        },
        {
            "rowid": 28,
            "columnid": 37,
            "hp": 0,
            "robottype": "ArmourSoldier",#铁甲兵
            "roleid": 11
        },
        {
            "rowid": 29,
            "columnid": 35,
            "hp": 0,
            "robottype": "ArmourSoldier",#铁甲兵
            "roleid": 12
        },
        {
            "rowid": 29,
            "columnid": 36,
            "hp": 0,
            "robottype": "ArmourSoldier",#铁甲兵
            "roleid": 13
        },
        {
            "rowid": 29,
            "columnid": 37,
            "hp": 0,
            "robottype": "ArmourSoldier",#铁甲兵
            "roleid": 14
        }
    ]
}